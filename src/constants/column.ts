export const recommendedCards = [
  {
    id: 1,
    name: "RECOMMENDED COLUMN",
    value: "オススメ",
  },
  {
    id: 2,
    name: "RECOMMENDED DIET",
    value: "ダイエット",
  },
  {
    id: 3,
    name: "RECOMMENDED BEAUTY",
    value: "美容",
  },
  {
    id: 4,
    name: "RECOMMENDED HEALTH",
    value: "健康",
  },
];

export const columnCards = [
  {
    id: 1,
    title: "魚を食べて頭もカラダも元気に！知っておきたい魚を食べるメリ…",
    tags: ["#魚料理", "#和食", "#DHA"],
    image: "./column-1.jpg",
    date: new Date(),
  },
  {
    id: 2,
    title: "魚を食べて頭もカラダも元気に！知っておきたい魚を食べるメリ…",
    tags: ["#魚料理", "#和食", "#DHA"],
    image: "./column-2.jpg",
    date: new Date(),
  },
  {
    id: 3,
    title: "魚を食べて頭もカラダも元気に！知っておきたい魚を食べるメリ…",
    tags: ["#魚料理", "#和食", "#DHA"],
    image: "./column-3.jpg",
    date: new Date(),
  },
  {
    id: 4,
    title: "魚を食べて頭もカラダも元気に！知っておきたい魚を食べるメリ…",
    tags: ["#魚料理", "#和食", "#DHA"],
    image: "./column-4.jpg",
    date: new Date(),
  },
  {
    id: 5,
    title: "魚を食べて頭もカラダも元気に！知っておきたい魚を食べるメリ…",
    tags: ["#魚料理", "#和食", "#DHA"],
    image: "./column-5.jpg",
    date: new Date(),
  },
  {
    id: 6,
    title: "魚を食べて頭もカラダも元気に！知っておきたい魚を食べるメリ…",
    tags: ["#魚料理", "#和食", "#DHA"],
    image: "./column-6.jpg",
    date: new Date(),
  },
  {
    id: 7,
    title: "魚を食べて頭もカラダも元気に！知っておきたい魚を食べるメリ…",
    tags: ["#魚料理", "#和食", "#DHA"],
    image: "./column-7.jpg",
    date: new Date(),
  },
  {
    id: 8,
    title: "魚を食べて頭もカラダも元気に！知っておきたい魚を食べるメリ…",
    tags: ["#魚料理", "#和食", "#DHA"],
    image: "./column-8.jpg",
    date: new Date(),
  },
];
