import { CategoryButtonProps } from "../components/CategoryButton";

interface ICategory extends CategoryButtonProps {
  id: number;
}

export const categories: ICategory[] = [
  {
    id: 1,
    icon: "knife",
    name: "Morning",
  },
  {
    id: 2,
    icon: "knife",
    name: "Lunch",
  },
  {
    id: 3,
    icon: "knife",
    name: "Dinner",
  },
  {
    id: 4,
    icon: "cup",
    name: "Snack",
  },
];

export const cardItems = [
  {
    id: 1,
    name: "05.21.Morning",
    image: "m01.jpg",
  },
  {
    id: 2,
    name: "05.21.Lunch",
    image: "l03.jpg",
  },
  {
    id: 3,
    name: "05.21.Dinner",
    image: "d01.jpg",
  },
  {
    id: 4,
    name: "05.21.Snack",
    image: "l01.jpg",
  },
  {
    id: 5,
    name: "05.20.Morning",
    image: "m01.jpg",
  },
  {
    id: 6,
    name: "05.20.Lunch",
    image: "l02.jpg",
  },
  {
    id: 7,
    name: "05.20.Dinner",
    image: "d02.jpg",
  },
  {
    id: 8,
    name: "05.21.Snack",
    image: "s01.jpg",
  },
];
