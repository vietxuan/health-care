export const recordCardItems = [
  {
    id: 1,
    name: "BODY RECORD",
    button: "自分のカラダの記録",
    image: "MyRecommend-1.jpg",
  },
  {
    id: 2,
    name: "MY EXERCISE",
    button: "自分の運動の記録",
    image: "MyRecommend-2.jpg",
  },
  {
    id: 3,
    name: "MY DIARY",
    button: "自分の日記",
    image: "MyRecommend-3.jpg",
  },
];

export const diaryData = {
  date: new Date(),
  content: `私の日記の記録が一部表示されます。
  テキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキスト…`,
};

export const exerciseData = {
  name: "家事全般（立位・軽い）",
  time: 10,
  kcal: 26,
};
