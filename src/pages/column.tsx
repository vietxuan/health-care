import Box from "@mui/material/Box";
import Container from "@mui/material/Container";
import Grid from "@mui/material/Grid";
import Button from "../components/Button";
import ColumnCard from "../components/ColumnCard";
import RecommendedCard from "../components/RecommendedCard";
import { columnCards, recommendedCards } from "../constants/column";

const ColumnPage = () => {
  return (
    <Container>
      <Grid sx={{ mt: 3 }} container rowSpacing={2} spacing={4}>
        {recommendedCards.map((recommendedCard) => (
          <Grid key={recommendedCard.id} item xs={12} sm={6} md={4} lg={3}>
            <RecommendedCard {...recommendedCard} />
          </Grid>
        ))}
      </Grid>
      <Grid sx={{ mt: 3 }} container rowSpacing="18px" spacing={1}>
        {columnCards.map((columnCard) => (
          <Grid key={columnCard.id} item xs={12} sm={6} md={4} lg={3}>
            <ColumnCard {...columnCard} />
          </Grid>
        ))}
      </Grid>
      <Box sx={{ display: "flex", mt: "26px", mb: 8 }}>
        <Button sx={{ mx: "auto" }}>コラムをもっと見る</Button>
      </Box>
    </Container>
  );
};

export default ColumnPage;
