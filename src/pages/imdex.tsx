import Box from "@mui/material/Box";
import Container from "@mui/material/Container";
import Grid from "@mui/material/Grid";
import Button from "../components/Button";
import Card from "../components/Card";
import CategoryButton from "../components/CategoryButton";
import Chart from "../components/Chart";
import CircularProgress from "../components/CircularProgress";
import { cardItems, categories } from "../constants/home";

function HomePage() {
  return (
    <Box>
      <Box sx={{ display: "flex" }}>
        <Box
          sx={{
            position: "relative",
            width: "42%",
            backgroundImage: "url('d01.jpg')",
            aspectRatio: "540 / 316",
            backgroundRepeat: "no-repeat",
            backgroundPosition: "center center",
            backgroundSize: "cover",
            display: "flex",
            alignItems: "center",
            justifyContent: "center",
          }}
        >
          <CircularProgress percent={75} date={new Date()} />
        </Box>
        <Box
          sx={{
            width: "58%",
            backgroundColor: "primary.dark",
            pl: "52px",
            pr: "102px",
            py: "12px",
          }}
        >
          <Chart />
        </Box>
      </Box>
      <Container sx={{ mt: "22px" }}>
        <Box
          sx={{
            display: "flex",
            justifyContent: "space-between",
            maxWidth: 788,
            mx: "auto",
          }}
        >
          {categories.map((category) => (
            <CategoryButton key={category.id} {...category} />
          ))}
        </Box>
        <Grid sx={{ mt: 3 }} container rowSpacing={1} spacing={1}>
          {cardItems.map((card) => (
            <Grid key={card.id} item xs={12} sm={6} md={4} lg={3}>
              <Card {...card} />
            </Grid>
          ))}
        </Grid>
        <Box sx={{ display: "flex", mt: "28px", mb: 8 }}>
          <Button sx={{ mx: "auto" }}>記録をもっと見る</Button>
        </Box>
      </Container>
    </Box>
  );
}

export default HomePage;
