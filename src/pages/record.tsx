import Box from "@mui/material/Box";
import Container from "@mui/material/Container";
import Grid from "@mui/material/Grid";
import Typography from "@mui/material/Typography";
import { FC } from "react";
import Button from "../components/Button";
import Chart from "../components/Chart";
import ChartButton from "../components/ChartButton";
import DiaryCard from "../components/DiaryCard";
import ExerciseItem from "../components/ExerciseItem";
import RecordCard from "../components/RecordCard";
import { diaryData, exerciseData, recordCardItems } from "../constants/record";

const RecordPage: FC = () => {
  return (
    <Container sx={{ mt: 7 }}>
      <Grid sx={{ mt: 3 }} container rowSpacing={[2, 4, 6]} spacing={[2, 4, 6]}>
        {recordCardItems.map((recordCard) => (
          <Grid key={recordCard.id} item xs={4}>
            <RecordCard {...recordCard} />
          </Grid>
        ))}
      </Grid>
      <Box
        sx={{
          mt: 7,
          backgroundColor: "secondary.dark",
          py: 2,
          px: 3,
        }}
      >
        <Box sx={{ display: "flex" }}>
          <Typography
            sx={{
              maxWidth: 96,
              fontFamily: "'Inter', sans-serif",
              fontWeight: 400,
              fontSize: 15,
              lineHeight: "18px",
              letterSpacing: "0.15px",
              color: "primary.light",
            }}
          >
            BODY RECORD
          </Typography>
          <Typography
            sx={{
              fontFamily: "'Inter', sans-serif",
              fontWeight: 400,
              fontSize: 22,
              lineHeight: "27px",
              letterSpacing: "0.11px",
              color: "primary.light",
            }}
          >
            2021.05.21
          </Typography>
        </Box>
        <Box sx={{ height: 185, mb: 1 }}>
          <Chart />
        </Box>
        <ChartButton
          variant="secondary"
          sx={{
            mr: 2,
          }}
        >
          日
        </ChartButton>
        <ChartButton
          variant="secondary"
          sx={{
            mr: 2,
          }}
        >
          週
        </ChartButton>
        <ChartButton
          variant="secondary"
          sx={{
            mr: 2,
          }}
        >
          月
        </ChartButton>
        <ChartButton>年</ChartButton>
      </Box>

      <Box
        sx={{
          mt: 7,
          backgroundColor: "secondary.dark",
          py: 2,
          px: 3,
        }}
      >
        <Box sx={{ display: "flex", mb: "4px" }}>
          <Typography
            sx={{
              maxWidth: 96,
              fontFamily: "'Inter', sans-serif",
              fontWeight: 400,
              fontSize: 15,
              lineHeight: "18px",
              letterSpacing: "0.15px",
              color: "primary.light",
            }}
          >
            MY EXERCISE
          </Typography>
          <Typography
            sx={{
              fontFamily: "'Inter', sans-serif",
              fontWeight: 400,
              fontSize: 22,
              lineHeight: "27px",
              letterSpacing: "0.11px",
              color: "primary.light",
            }}
          >
            2021.05.21
          </Typography>
        </Box>
        <Box
          sx={{
            height: 192,
            overflowY: "auto",
            "::-webkit-scrollbar": {
              width: 6,
            },
            "::-webkit-scrollbar-track": {
              backgroundColor: "secondary.light",
              borderRadius: 3,
            },
            "::-webkit-scrollbar-thumb": {
              backgroundColor: "#ffcc21",
              borderRadius: 3,
            },
          }}
        >
          <Grid container spacing="40px" rowSpacing={1} sx={{ pr: "45px" }}>
            {new Array(16).fill(exerciseData).map((item, index) => (
              <Grid key={index} item xs={6}>
                <ExerciseItem {...item} />
              </Grid>
            ))}
          </Grid>
        </Box>
      </Box>
      <Grid sx={{ mt: 3 }} container rowSpacing="12px" spacing="12px">
        {new Array(8).fill(diaryData).map((card, index) => (
          <Grid key={index} item xs={12} sm={6} md={4} lg={3}>
            <DiaryCard {...card} />
          </Grid>
        ))}
      </Grid>
      <Box sx={{ display: "flex", mt: "30px", mb: 8 }}>
        <Button sx={{ mx: "auto" }}>自分の日記をもっと見る</Button>
      </Box>
    </Container>
  );
};

export default RecordPage;
