import CssBaseline from "@mui/material/CssBaseline";
import { ThemeProvider } from "@mui/material/styles";
import { Routes, Route } from "react-router-dom";
import Layout from "./layouts";
import ColumnPage from "./pages/column";
import HomePage from "./pages/imdex";
import RecordPage from "./pages/record";
import theme from "./styles/theme";

function App() {
  return (
    <ThemeProvider theme={theme}>
      <CssBaseline />
      <Layout>
        <Routes>
          <Route path="/" element={<HomePage />} />
          <Route path="/record" element={<RecordPage />} />
          <Route path="/column" element={<ColumnPage />} />
        </Routes>
      </Layout>
    </ThemeProvider>
  );
}

export default App;
