import Box from "@mui/material/Box";
import { FC, PropsWithChildren } from "react";
import Footer from "../components/Footer";
import Header from "../components/Header";
import ScrollTop from "../components/ScrollTop";

const Layout: FC<PropsWithChildren<{}>> = ({ children }) => {
  return (
    <Box sx={{ minHeight: "100vh", display: "flex", flexDirection: "column" }}>
      <div id="back-to-top-anchor" />
      <Header />
      <Box>{children}</Box>
      <ScrollTop />
      <Footer />
    </Box>
  );
};

export default Layout;
