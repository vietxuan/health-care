import ButtonBase, { ButtonBaseProps } from "@mui/material/ButtonBase";
import { FC } from "react";

interface ChartButtonProps extends ButtonBaseProps {
  variant?: "primary" | "secondary";
}

const ChartButton: FC<ChartButtonProps> = ({
  variant = "primary",
  sx,
  ...props
}) => (
  <ButtonBase
    sx={{
      backgroundColor: variant === "primary" ? "#FFCC21" : "primary.light",
      borderRadius: "11px",
      minWidth: 56,
      fontFamily: "'Noto Sans JP', sans-serif",
      fontWeight: 300,
      fontSize: 15,
      lineHight: "22px",
      textAlign: "center",
      letterSpacing: "0.075px",
      color: variant === "primary" ? "primary.light" : "#FFCC21",
      py: "1px",
      ...sx,
    }}
    {...props}
  />
);

export default ChartButton;
