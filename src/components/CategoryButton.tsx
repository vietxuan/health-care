import Box from "@mui/material/Box";
import ButtonBase from "@mui/material/ButtonBase";
import Typography from "@mui/material/Typography";
import { FC } from "react";
import Icon, { IconName } from "./Icon";

export interface CategoryButtonProps {
  icon: IconName;
  name: string;
}

const CategoryButton: FC<CategoryButtonProps> = ({ icon, name }) => (
  <ButtonBase
    sx={{
      position: "relative",
      display: "flex",
      justifyContent: "center",
      alignItems: "center",
    }}
  >
    <Icon name="hex" />
    <Box
      sx={{
        position: "absolute",
        display: "flex",
        justifyContent: "center",
        alignItems: "center",
        flexDirection: "column",
      }}
    >
      <Icon name={icon} />
      <Typography
        sx={{
          fontFamily: "'Inter', sans-serif",
          fontWeight: 400,
          fontSize: 20,
          lineHeight: "24px",
          textAlign: "center",
          color: "primary.light",
        }}
      >
        {name}
      </Typography>
    </Box>
  </ButtonBase>
);

export default CategoryButton;
