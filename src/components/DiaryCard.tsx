import Box from "@mui/material/Box";
import Typography from "@mui/material/Typography";
import { FC } from "react";
import Moment from "react-moment";

interface DiaryCardProps {
  date: Date;
  content: string;
}

const DiaryCard: FC<DiaryCardProps> = ({ date, content }) => {
  return (
    <Box
      sx={{
        pt: 2,
        px: 2,
        pb: "27px",
        border: "2px solid #707070",
        backgroundColor: "primary.light",
      }}
    >
      <Box sx={{ mb: 1 }}>
        <Typography
          sx={{
            fontFamily: "'Inter', sans-serif",
            fontWeight: 400,
            fontSize: 18,
            lineHeight: "22px",
            letterSpacing: "0.09px",
            color: "secondary.dark",
          }}
        >
          <Moment format="YY.MM.DD">{date}</Moment>
        </Typography>
        <Typography
          sx={{
            fontFamily: "'Inter', sans-serif",
            fontWeight: 400,
            fontSize: 18,
            lineHeight: "22px",
            letterSpacing: "0.09px",
            color: "secondary.dark",
          }}
        >
          <Moment format="hh:mm">{date}</Moment>
        </Typography>
      </Box>
      <Typography
        sx={{
          fontFamily: "'Noto Sans JP', sans-serif",
          fontWeight: 300,
          fontSize: 12,
          lineHeight: "17px",
          letterSpacing: "0.06px",
          color: "secondary.dark",
        }}
      >
        {content}
      </Typography>
    </Box>
  );
};

export default DiaryCard;
