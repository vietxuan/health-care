import Box from "@mui/material/Box";
import Typography from "@mui/material/Typography";
import { FC } from "react";

interface ExerciseItemProps {
  name: string;
  kcal: number;
  time: number;
}

const ExerciseItem: FC<ExerciseItemProps> = ({ name, kcal, time }) => (
  <Box
    sx={{
      pl: 1,
      position: "relative",
      borderBottom: "1px solid #777777",
      pb: "2px",
      ":before": {
        position: "absolute",
        content: '"●"',
        left: 0,
        fontWeight: 300,
        fontSize: 5,
        lineHeight: "7px",
        mt: "7px",
        color: "primary.light",
      },
    }}
  >
    <Box sx={{ display: "flex", justifyContent: "space-between" }}>
      <Typography
        sx={{
          fontFamily: "'Noto Sans JP', sans-serif",
          fontWeight: 300,
          fontSize: 15,
          lineHeight: "22px",
          letterSpacing: "0.075px",
          color: "primary.light",
        }}
      >
        {name}
      </Typography>
      <Typography
        sx={{
          fontFamily: "'Inter', sans-serif",
          fontWeight: 400,
          fontSize: 18,
          lineHeight: "22px",
          letterSpacing: "0.09px",
          color: "primary.main",
        }}
      >
        {time} min
      </Typography>
    </Box>
    <Typography
      sx={{
        fontFamily: "'Inter', sans-serif",
        fontWeight: 400,
        fontSize: 15,
        lineHeight: "28px",
        letterSpacing: "0.075px",
        color: "primary.main",
      }}
    >
      {kcal}kcal
    </Typography>
  </Box>
);

export default ExerciseItem;
