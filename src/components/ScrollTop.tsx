import Box from "@mui/material/Box";
import IconButton from "@mui/material/IconButton";
import useScrollTrigger from "@mui/material/useScrollTrigger";
import Zoom from "@mui/material/Zoom";
import { FC, MouseEvent } from "react";
import Icon from "./Icon";

interface ScrollTopProps {
  window?: () => Window;
}

const ScrollTop: FC<ScrollTopProps> = ({ window }) => {
  const trigger = useScrollTrigger({
    target: window ? window() : undefined,
    disableHysteresis: true,
    threshold: 100,
  });

  const handleClick = (event: MouseEvent<HTMLDivElement>) => {
    const anchor = (
      (event.target as HTMLDivElement).ownerDocument || document
    ).querySelector("#back-to-top-anchor");
    if (anchor) {
      anchor.scrollIntoView({
        behavior: "smooth",
        block: "center",
      });
    }
  };

  return (
    <Zoom in={trigger}>
      <Box
        onClick={handleClick}
        role="presentation"
        sx={{
          position: "fixed",
          bottom: 16,
          right: 16,
        }}
      >
        <IconButton
          sx={{
            backgroundColor: "rgba(255, 255, 255, 0.0001)",
            border: "1px solid #777777",
            width: 48,
            height: 48,
            borderRadius: 48,
            display: "flex",
            justifyContent: "center",
            alignItems: "center",
          }}
        >
          <Icon name="arrow-up" />
        </IconButton>
      </Box>
    </Zoom>
  );
};

export default ScrollTop;
