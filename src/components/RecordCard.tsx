import Box from "@mui/material/Box";
import ButtonBase from "@mui/material/ButtonBase";
import Typography from "@mui/material/Typography";
import { FC } from "react";

interface RecordCardProps {
  image: string;
  name: string;
  button: string;
}

const RecordCard: FC<RecordCardProps> = ({ image, name, button }) => {
  return (
    <Box
      sx={{
        position: "relative",
        aspectRatio: "1",
        borderWidth: 24,
        borderColor: "primary.main",
        borderStyle: "solid",
        backgroundColor: "#000000",
        display: "flex",
        justifyContent: "center",
        alignItems: "center",
      }}
    >
      <Box
        sx={{
          position: "absolute",
          inset: 0,
          backgroundImage: `url('${image}')`,
          backgroundRepeat: "no-repeat",
          backgroundPosition: "center center",
          backgroundSize: "cover",
          display: "flex",
          alignItems: "center",
          justifyContent: "center",
          filter: "grayscale(100%)",
          opacity: 0.3,
        }}
      />
      <Box
        sx={{
          position: "absolute",
          display: "flex",
          alignItems: "center",
          flexDirection: "column",
        }}
      >
        <Typography
          sx={{
            fontFamily: "'Inter', sans-serif",
            fontWeight: 400,
            fontSize: 24,
            lineHeight: "30px",
            letterSpacing: "0.125px",
            color: "primary.main",
          }}
        >
          {name}
        </Typography>
        <ButtonBase
          sx={{
            mt: "11px",
            backgroundColor: "secondary.main",
            fontFamily: "'Noto Sans JP', sans-serif",
            fontWeight: 300,
            fontSize: 14,
            lineHeight: "20px",
            textAlign: "center",
            color: "primary.light",
            minWidth: 160,
            py: "2px",
          }}
        >
          {button}
        </ButtonBase>
      </Box>
    </Box>
  );
};

export default RecordCard;
