import Box from "@mui/material/Box";
import Typography from "@mui/material/Typography";
import { FC } from "react";
import Moment from "react-moment";

interface CircularProgressProps {
  percent: number;
  date: Date;
}

const CircularProgress: FC<CircularProgressProps> = ({ percent, date }) => {
  return (
    <Box
      sx={{
        display: "flex",
        position: "relative",
        justifyContent: "center",
        alignItems: "center",
        svg: {
          width: 181,
          height: 181,
          transform: "rotate(-90deg)",
          circle: {
            width: "100%",
            height: "100%",
            fill: "none",
            strokeWidth: 3,
            strokeDasharray: "559px",
            strokeDashoffset: `calc(559px - (559px * ${percent}) / 100)`,
            stroke: "#FFFFFF",
            filter: "drop-shadow( 0px 0px 6px #FC7400)",
          },
        },
      }}
    >
      <svg>
        <circle cx="90.5" cy="90.5" r="89"></circle>
      </svg>
      <Box
        sx={{
          position: "absolute",
          display: "flex",
          flexDirection: "row",
          alignItems: "end",
        }}
      >
        <Typography
          sx={{
            mr: "4px",
            fontFamily: "'Inter', sans-serif",
            fontWeight: 400,
            fontSize: 18,
            lineHeight: "22px",
            textAlign: "center",
            color: "primary.light",
            textShadow: "0px 0px 6px #FC7400",
          }}
        >
          <Moment format="DD/MM">{date}</Moment>
        </Typography>
        <Typography
          sx={{
            fontFamily: "'Inter', sans-serif",
            fontWeight: 400,
            fontSize: 25,
            lineHeight: "30px",
            textAlign: "center",
            color: "primary.light",
            textShadow: "0px 0px 6px #FCA500",
          }}
        >
          {percent}%
        </Typography>
      </Box>
    </Box>
  );
};

export default CircularProgress;
