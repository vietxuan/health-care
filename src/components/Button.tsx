import ButtonBase, { ButtonBaseProps } from "@mui/material/ButtonBase";
import { FC } from "react";

const Button: FC<ButtonBaseProps> = ({ sx, ...props }) => {
  return (
    <ButtonBase
      sx={{
        px: "4px",
        pt: "14px",
        pb: "16px",
        color: "primary.light",
        fontFamily: "'Noto Sans JP', sans-serif",
        fontWeight: 300,
        fontSize: 18,
        lineHeight: "26px",
        background: "linear-gradient(32.95deg, #FFCC21 8.75%, #FF963C 86.64%);",
        minWidth: 296,
        borderRadius: 1,
        ...sx,
      }}
      {...props}
    />
  );
};

export default Button;
