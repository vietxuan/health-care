import Box from "@mui/material/Box";
import CardMUI from "@mui/material/Card";
import CardActionArea from "@mui/material/CardActionArea";
import CardContent from "@mui/material/CardContent";
import CardMedia from "@mui/material/CardMedia";
import Typography from "@mui/material/Typography";
import { FC } from "react";
import Moment from "react-moment";

interface ColumnCardProps {
  title: string;
  image: string;
  date: Date;
  tags: string[];
}

const ColumnCard: FC<ColumnCardProps> = ({ title, image, date, tags }) => {
  return (
    <CardMUI
      sx={{
        borderRadius: 0,
        boxShadow: "unset",
      }}
    >
      <CardActionArea>
        <Box sx={{ position: "relative" }}>
          <CardMedia
            component="img"
            image={image}
            alt={title}
            sx={{
              aspectRatio: "234 / 144",
            }}
          />
          <Box
            sx={{
              position: "absolute",
              display: "flex",
              bottom: 0,
              backgroundColor: "primary.main",
              px: 1,
              fontFamily: "'Inter', sans-serif",
              fontWeight: 400,
              fontSize: 15,
              lineHeight: "30px",
              color: "primary.light",
            }}
          >
            <Typography sx={{ mr: 2 }}>
              <Moment format="YY.MM.DD">{date}</Moment>
            </Typography>
            <Typography>
              <Moment format="hh:mm">{date}</Moment>
            </Typography>
          </Box>
        </Box>
        <CardContent sx={{ mt: 1, p: 0 }}>
          <Typography
            gutterBottom
            variant="h5"
            component="div"
            sx={{
              fontFamily: "'Noto Sans JP', sans-serif",
              fontWeight: 300,
              fontSize: 15,
              lineHeight: "22px",
              letterSpacing: "0.075px",
              color: "secondary.dark",
              mb: 0,
            }}
          >
            {title}
          </Typography>
          <Typography
            variant="body2"
            color="text.secondary"
            sx={{
              fontFamily: "'Noto Sans JP', sans-serif",
              fontWeight: 300,
              fontSize: "12px",
              lineHeight: "22px",
              letterSpacing: "0.06px",
              color: "secondary.main",
            }}
          >
            {tags.join(" ")}
          </Typography>
        </CardContent>
      </CardActionArea>
    </CardMUI>
  );
};

export default ColumnCard;
