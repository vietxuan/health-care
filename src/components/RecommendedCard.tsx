import Box from "@mui/material/Box";
import Typography from "@mui/material/Typography";
import { FC } from "react";

interface RecommendedCardProps {
  name: string;
  value: string;
}

const RecommendedCard: FC<RecommendedCardProps> = ({ name, value }) => {
  return (
    <Box
      sx={{
        display: "flex",
        flexDirection: "column",
        justifyContent: "center",
        alignItems: "center",
        aspectRatio: "216 / 144",
        backgroundColor: "primary.dark",
        px: 8,
        py: 3,
      }}
    >
      <Typography
        sx={{
          fontFamily: "'Inter', sans-serif",
          fontWeight: 400,
          fontSize: 22,
          lineHeight: "27px",
          textAlign: "center",
          letterSpacing: "0.11px",
          color: "primary.main",
        }}
      >
        {name}
      </Typography>
      <Box
        sx={{
          width: "56px",
          height: "1px",
          backgroundColor: "primary.light",
          mb: 1,
          mt: "10px",
        }}
      />
      <Typography
        sx={{
          fontFamily: "'Noto Sans JP', sans-serif",
          fontWeight: 300,
          fontSize: 18,
          lineHeight: "26px",
          textAlign: "center",
          color: "primary.light",
        }}
      >
        {value}
      </Typography>
    </Box>
  );
};

export default RecommendedCard;
