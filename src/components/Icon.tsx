import { FC } from "react";

export type IconName =
  | "memo"
  | "challenge"
  | "info"
  | "menu"
  | "close"
  | "logo"
  | "arrow-up"
  | "hex"
  | "knife"
  | "cup";

interface IconProps {
  name: IconName;
}

const Icon: FC<IconProps> = ({ name }) => {
  return <img src={`./${name}.svg`} alt={name} />;
};

export default Icon;
