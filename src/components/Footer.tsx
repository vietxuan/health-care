import Box from "@mui/material/Box";
import Container from "@mui/material/Container";
import Typography from "@mui/material/Typography";
import { FC } from "react";

const footerItems = [
  {
    id: 1,
    name: "会員登録",
  },
  {
    id: 2,
    name: "運営会社",
  },
  {
    id: 3,
    name: "利用規約",
  },
  {
    id: 4,
    name: "個人情報の取扱について",
  },
  {
    id: 5,
    name: "特定商取引法に基づく表記",
  },
  {
    id: 6,
    name: "お問い合わせ",
  },
];

interface FooterItemProps {
  name: string;
}

const FooterItem: FC<FooterItemProps> = ({ name }) => (
  <Box
    component="li"
    sx={{
      listStyle: "none",
      mr: "45px",
      ":last-child": {
        mr: 0,
      },
    }}
  >
    <Typography
      sx={{
        fontFamily: "'Noto Sans JP', sans-serif",
        fontWeight: 300,
        fontSize: 11,
        lineHeight: "16px",
        letterSpacing: "0.033px",
        color: "primary.light",
      }}
    >
      {name}
    </Typography>
  </Box>
);

const Footer: FC = () => {
  return (
    <Box
      component="footer"
      sx={{
        mt: "auto",
        backgroundColor: "secondary.dark",
        py: 7,
      }}
    >
      <Container>
        <Box
          component="ul"
          sx={{
            display: "flex",
            flexDirection: "row",
            listStyleType: "none",
            p: 0,
            m: 0,
          }}
        >
          {footerItems.map((item) => (
            <FooterItem key={item.id} name={item.name} />
          ))}
        </Box>
      </Container>
    </Box>
  );
};

export default Footer;
