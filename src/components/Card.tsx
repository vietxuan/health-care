import Typography from "@mui/material/Typography";
import { FC } from "react";
import CardMUI from "@mui/material/Card";
import CardMedia from "@mui/material/CardMedia";
import CardActionArea from "@mui/material/CardActionArea";

interface CardProps {
  name: string;
  image: string;
}

const Card: FC<CardProps> = ({ name, image }) => {
  return (
    <CardMUI
      sx={{
        borderRadius: 0,
        boxShadow: "unset",
      }}
    >
      <CardActionArea>
        <CardMedia
          component="img"
          image={image}
          alt={name}
          sx={{
            aspectRatio: "1",
          }}
        />
        <Typography
          sx={{
            position: "absolute",
            bottom: 0,
            fontFamily: "'Inter', sans-serif",
            fontWeight: 400,
            fontSize: 15,
            lineHeight: "18px",
            letterSpacing: "0.15px",
            color: "primary.light",
            backgroundColor: "primary.main",
            width: "50%",
            py: "7px",
            px: 1,
          }}
        >
          {name}
        </Typography>
      </CardActionArea>
    </CardMUI>
  );
};

export default Card;
