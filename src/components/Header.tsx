import Badge, { BadgeProps } from "@mui/material/Badge";
import Box from "@mui/material/Box";
import ButtonBase from "@mui/material/ButtonBase";
import Container from "@mui/material/Container";
import IconButton from "@mui/material/IconButton";
import { styled } from "@mui/material/styles";
import Typography from "@mui/material/Typography";
import { FC, ReactNode, useRef, useState } from "react";
import { useOnClickOutside } from "usehooks-ts";
import Icon, { IconName } from "./Icon";
import { useLocation } from "react-router-dom";

const menuItems = [
  {
    id: 1,
    name: "自分の記録",
  },
  {
    id: 2,
    name: "体重グラフ",
  },
  {
    id: 3,
    name: "目標",
  },
  {
    id: 4,
    name: "選択中のコース",
  },
  {
    id: 5,
    name: "コラム一覧",
    href: "/column",
  },
  {
    id: 6,
    name: "設定",
  },
];

const StyledBadge = styled(Badge)<BadgeProps>({
  "& .MuiBadge-badge": {
    top: 8,
    backgroundColor: "#EA6C00",
    fontWeight: 400,
    fontSize: 10,
    lineHeight: "12px",
    textAlign: "center",
    color: "primary.light",
    width: 16,
    height: 16,
    minWidth: 16,
  },
});

interface NavItemProps {
  icon: IconName;
  name: string;
  href?: string;
  badgeContent?: ReactNode;
}

const NavItem: FC<NavItemProps> = ({ icon, name, href, badgeContent }) => {
  const location = useLocation();
  const active = location.pathname === href;
  return (
    <Box
      component="li"
      sx={{
        listStyle: "none",
      }}
    >
      <ButtonBase
        sx={{
          p: "11px",
          ":hover": {
            ".MuiTypography-root": {
              color: "secondary.main",
            },
          },
        }}
        {...(href && { href })}
      >
        {badgeContent ? (
          <StyledBadge badgeContent={badgeContent}>
            <Icon name={icon} />
          </StyledBadge>
        ) : (
          <img src={`./${icon}.svg`} alt={icon} />
        )}
        <Typography
          sx={{
            ml: 1,
            fontFamily: "'Noto Sans JP', sans-serif",
            fontWeight: 300,
            fontSize: 16,
            lineHeight: "23px",
            color: active ? "secondary.main" : "primary.light",
            transition: "color 0.3s",
          }}
        >
          {name}
        </Typography>
      </ButtonBase>
    </Box>
  );
};

interface MenuItemProps {
  name: string;
  onClick?: () => void;
  href?: string;
}

const MenuItem: FC<MenuItemProps> = ({ name, onClick, href }) => (
  <ButtonBase
    {...(href && { href })}
    onClick={onClick}
    sx={{
      position: "relative",
      pl: "32px",
      py: "23px",
      backgroundColor: "secondary.light",
      width: 280,
      justifyContent: "left",
      ":before": {
        content: '" "',
        position: "absolute",
        top: 0,
        left: 0,
        right: 0,
        height: "1px",
        backgroundColor: "primary.light",
        mixBlendMode: "normal",
        opacity: 0.15,
        zIndex: 1,
      },
      ":after": {
        content: '" "',
        position: "absolute",
        bottom: 0,
        left: 0,
        right: 0,
        height: "1px",
        backgroundColor: "primary.dark",
        mixBlendMode: "normal",
        opacity: 0.25,
        zIndex: 1,
      },
      ":hover": {
        ".MuiTypography-root": {
          color: "secondary.main",
        },
      },
    }}
  >
    <Typography
      sx={{
        fontFamily: "'Noto Sans JP', sans-serif",
        fontWeight: 300,
        fontSize: 18,
        lineHeight: "26px",
        color: "primary.light",
        transition: "color 0.3s",
      }}
    >
      {name}
    </Typography>
  </ButtonBase>
);

const Header: FC = () => {
  const ref = useRef(null);
  const [open, setOpen] = useState<boolean>(false);
  const handleChange = () => setOpen((prev) => !prev);
  const onClose = () => setOpen(false);
  useOnClickOutside(ref, onClose);
  return (
    <Box
      component="header"
      sx={{
        backgroundColor: "secondary.dark",
        boxShadow: "0px 3px 6px rgba(0, 0, 0, 0.160784)",
      }}
    >
      <Container sx={{ alignItems: "center", display: "flex" }}>
        <ButtonBase sx={{ mr: "auto" }} href="/">
          <img src="./logo.svg" alt="logo" />
        </ButtonBase>
        <Box
          component="ul"
          sx={{
            display: "flex",
            flexDirection: "row",
            listStyleType: "none",
            p: 0,
            m: 0,
          }}
        >
          <NavItem icon="memo" name="自分の記録" href="/record" />
          <NavItem icon="challenge" name="チャレンジ" />
          <NavItem icon="info" name="お知らせ" badgeContent={4} />
        </Box>
        <Box ref={ref} sx={{ position: "relative" }}>
          <IconButton sx={{ ml: 2 }} onClick={handleChange}>
            <Icon name={open ? "close" : "menu"} />
          </IconButton>
          {open && (
            <Box
              sx={{
                position: "absolute",
                top: 40,
                left: -224,
                zIndex: 1,
              }}
            >
              {menuItems.map((item) => (
                <MenuItem key={item.id} onClick={onClose} {...item} />
              ))}
            </Box>
          )}
        </Box>
      </Container>
    </Box>
  );
};

export default Header;
